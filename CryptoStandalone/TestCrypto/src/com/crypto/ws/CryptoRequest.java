/**
 * 
 */
package com.crypto.ws;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMDecryptorProvider;
import org.bouncycastle.openssl.PEMEncryptedKeyPair;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JcePEMDecryptorProviderBuilder;

import com.crypto.beans.BeanEntrada;


/**
 * @author everis
 *
 */
public class CryptoRequest {

	private static final String SEPARADOR = "|";
	private static final String PRIVATE_KEY_PATH = "/arquitecturaAgave/DistV1/Configuracion/keys/llaves/VALTIERRA.cve";
	private static final String PASS_PHRASE = "VaL$3ierr78E";
	private static final String CERTIFICATE_PATH = "/arquitecturaAgave/DistV1/Configuracion/keys/llaves/00000100000100012377.cer";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Security.addProvider(new BouncyCastleProvider());
		CryptoRequest crypto = new CryptoRequest();
		try {
			//Se obtiene el PrivateKey del archivo .cve
			Reader reader = new FileReader(new File (PRIVATE_KEY_PATH));
			PrivateKey privateKey = crypto.getPrivateKeyDecrypt(reader, PASS_PHRASE);
			
			//Realiza la firma
			java.security.Signature signer = java.security.Signature.getInstance("SHA256withRSA");
			signer.initSign(privateKey);
			
			BeanEntrada entrada = new BeanEntrada();
			entrada.setFechaHora("14/12/2016 18:01:08");
			entrada.setIdUsuario("DANIEL VALTIERRA JIMENEZ");
			entrada.setIdentificador("AAMC260505MCMLTH00");
			entrada.setTipoPersona("fisica");
			StringBuilder cadenaOriginalDelMensajeDeConsulta = new StringBuilder(SEPARADOR);
			cadenaOriginalDelMensajeDeConsulta.append(entrada.getFechaHora()).append(SEPARADOR)
					.append(entrada.getIdUsuario()).append(SEPARADOR).append(entrada.getIdentificador())
					.append(SEPARADOR).append(entrada.getTipoPersona()).append(SEPARADOR);

			byte[] message = cadenaOriginalDelMensajeDeConsulta.toString().getBytes();
			signer.update(message);
			byte [] signature = signer.sign();
			byte [] result = Base64.encodeBase64(signature);
			String resultB64 = new String(result);
			System.out.println("CADENA ORIGINAL DE CONSULTA: " + cadenaOriginalDelMensajeDeConsulta);
			System.out.println("FIRMA EN BASE 64: " + resultB64);
			
			entrada.setFirma(new String(resultB64));
			
			BigInteger numeroSerieCertificado = crypto.getIdCertificate(CERTIFICATE_PATH);
			entrada.setNumSerieCrt(String.valueOf(numeroSerieCertificado));
			System.out.println("NUMERO DE SERIE DEL CERTIFICADO: " + entrada.getNumSerieCrt());
			entrada.setInfoFirmado("SHA256withRSA");
			System.out.println(" ====== VALORES DE ENTRADA PARA EL WS ======");
			System.out.println(entrada);
			System.out.println("===================================");
		} catch (IOException | NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Obtiene la Llave Privada apartir del archivo .cve
	 * @param privateKeyReader
	 * @param passPhrase
	 * @return
	 * @throws IOException
	 */
	public PrivateKey getPrivateKeyDecrypt(Reader privateKeyReader, String passPhrase) throws IOException{
		PEMParser pemParser = null;
		PrivateKeyInfo keyInfo = null;
		try {
			pemParser = new PEMParser(privateKeyReader);
			Object keyPair = pemParser.readObject();
			if (keyPair instanceof PEMEncryptedKeyPair){
				if (null == passPhrase){
					System.err.println("Key es incriptado, pero no tiene informado el passPhrase");
				}
				PEMDecryptorProvider decryptor = new JcePEMDecryptorProviderBuilder().build(passPhrase.toCharArray());
				PEMKeyPair decrypterKeyPair = ((PEMEncryptedKeyPair) keyPair).decryptKeyPair(decryptor);
				keyInfo = decrypterKeyPair.getPrivateKeyInfo();
			}else{
				keyInfo = ((PEMKeyPair) keyPair).getPrivateKeyInfo();
			}
		} catch (IOException e) {
			System.err.println("No se puede leer archivo");
			e.printStackTrace();
		}finally{
			pemParser.close();
		}
		
		PrivateKey privateKey = new JcaPEMKeyConverter().getPrivateKey(keyInfo);
		System.out.println("ALGORITMO LLAVE PRIVADA: " + privateKey.getAlgorithm());
		System.out.println("FORMATO LLAVE PRIVADA: " + privateKey.getFormat());
		return privateKey;
	}
	
	/**
	 * Obtiene al numero de serie del certificado
	 * @param certPath
	 * @return
	 * @throws IOException
	 */
	public BigInteger getIdCertificate(String certPath) throws IOException{
		FileInputStream inputStream = null;
		X509Certificate cer = null;
		try {
			CertificateFactory fact = CertificateFactory.getInstance("X.509");
			inputStream = new FileInputStream(certPath);
			cer = (X509Certificate)fact.generateCertificate(inputStream);
		} catch (CertificateException | FileNotFoundException e) {
			e.printStackTrace();
		}finally{
			inputStream.close();
		}
		return cer.getSerialNumber();
	}
}