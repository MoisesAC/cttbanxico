package com.crypto.beans;

public class BeanSalida {
	String advertenciaCliente;
	String cliente;
	String codigoRespuesta;
	String cveCifrada;
	String fechaHora;
	String infoFirmado;
	String numSerieCrt;
	String xmlCifrado;
	String firma;
	String mensajeRespuesta;
	/**
	 * @return the mensajeRespuesta
	 */
	public String getMensajeRespuesta() {
		return mensajeRespuesta;
	}
	/**
	 * @param mensajeRespuesta the mensajeRespuesta to set
	 */
	public void setMensajeRespuesta(String mensajeRespuesta) {
		this.mensajeRespuesta = mensajeRespuesta;
	}
	/**
	 * @return the firma
	 */
	public String getFirma() {
		return firma;
	}
	/**
	 * @param firma the firma to set
	 */
	public void setFirma(String firma) {
		this.firma = firma;
	}
	/**
	 * @return the advertenciaCliente
	 */
	public String getAdvertenciaCliente() {
		return advertenciaCliente;
	}
	/**
	 * @param advertenciaCliente the advertenciaCliente to set
	 */
	public void setAdvertenciaCliente(String advertenciaCliente) {
		this.advertenciaCliente = advertenciaCliente;
	}
	/**
	 * @return the cliente
	 */
	public String getCliente() {
		return cliente;
	}
	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	/**
	 * @return the codigoRespuesta
	 */
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	/**
	 * @param codigoRespuesta the codigoRespuesta to set
	 */
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	/**
	 * @return the cveCifrada
	 */
	public String getCveCifrada() {
		return cveCifrada;
	}
	/**
	 * @param cveCifrada the cveCifrada to set
	 */
	public void setCveCifrada(String cveCifrada) {
		this.cveCifrada = cveCifrada;
	}
	/**
	 * @return the fechaHora
	 */
	public String getFechaHora() {
		return fechaHora;
	}
	/**
	 * @param fechaHora the fechaHora to set
	 */
	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}
	/**
	 * @return the infoFirmado
	 */
	public String getInfoFirmado() {
		return infoFirmado;
	}
	/**
	 * @param infoFirmado the infoFirmado to set
	 */
	public void setInfoFirmado(String infoFirmado) {
		this.infoFirmado = infoFirmado;
	}
	/**
	 * @return the numSerieCrt
	 */
	public String getNumSerieCrt() {
		return numSerieCrt;
	}
	/**
	 * @param numSerieCrt the numSerieCrt to set
	 */
	public void setNumSerieCrt(String numSerieCrt) {
		this.numSerieCrt = numSerieCrt;
	}
	/**
	 * @return the xmlCifrado
	 */
	public String getXmlCifrado() {
		return xmlCifrado;
	}
	/**
	 * @param xmlCifrado the xmlCifrado to set
	 */
	public void setXmlCifrado(String xmlCifrado) {
		this.xmlCifrado = xmlCifrado;
	}
	
}
