package com.crypto.beans;

public class BeanEntrada {
	/**
	 * Fecha y Hora
	 */
	String fechaHora;
	/**
	 * Id Usuario
	 */
	String idUsuario;
	
	/**
	 * Identificador
	 */
	String identificador;
	/**
	 * TipoPersona
	 */
	String tipoPersona;
	/**
	 * Num Serie
	 */
	String numSerieCrt;
	/**
	 * Infor Firmado
	 */
	String infoFirmado;
	/**
	 * Firma
	 */
	String firma;
	
	
	/**
	 * @return the fechaHora
	 */
	public String getFechaHora() {
		return fechaHora;
	}
	/**
	 * @param fechaHora the fechaHora to set
	 */
	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}
	/**
	 * @return the idUsuario
	 */
	public String getIdUsuario() {
		return idUsuario;
	}
	/**
	 * @param idUsuario the idUsuario to set
	 */
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	/**
	 * @return the identificador
	 */
	public String getIdentificador() {
		return identificador;
	}
	/**
	 * @param identificador the identificador to set
	 */
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}
	/**
	 * @return the tipoPersona
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}
	/**
	 * @param tipoPersona the tipoPersona to set
	 */
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	/**
	 * @return the numSerieCrt
	 */
	public String getNumSerieCrt() {
		return numSerieCrt;
	}
	/**
	 * @param numSerieCrt the numSerieCrt to set
	 */
	public void setNumSerieCrt(String numSerieCrt) {
		this.numSerieCrt = numSerieCrt;
	}
	/**
	 * @return the infoFirmado
	 */
	public String getInfoFirmado() {
		return infoFirmado;
	}
	/**
	 * @param infoFirmado the infoFirmado to set
	 */
	public void setInfoFirmado(String infoFirmado) {
		this.infoFirmado = infoFirmado;
	}
	/**
	 * @return the firma
	 */
	public String getFirma() {
		return firma;
	}
	/**
	 * @param firma the firma to set
	 */
	public void setFirma(String firma) {
		this.firma = firma;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeanEntrada [\nfechaHora=" + fechaHora + ",\n idUsuario=" + idUsuario + ",\n identificador=" + identificador
				+ ",\n tipoPersona=" + tipoPersona + ",\n numSerieCrt=" + numSerieCrt + ",\n infoFirmado=" + infoFirmado
				+ ",\n firma=" + firma + "\n]";
	}

}
