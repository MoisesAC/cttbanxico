package com.crypto.launch;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import org.apache.commons.codec.binary.Base64;

import com.crypto.beans.BeanEntrada;
import com.crypto.beans.BeanSalida;

public class LaunchCrypto {
	
	private static final String PASS_KEYSTORE = "VaL$3ierr78E";
	private static final String TIPO_KEYSTORE = "JKS";
	private static final String ALIAS_KEYSTORE = "ATM";
	private static final String RUTA_JKS_LLAVE_PRIVADA = "/arquitecturaAgave/DistV1/Configuracion/keys/llaves/VALTIERRA.cve";
	//private static final String RUTA_JKS_LLAVE_PUBLICA = "/arquitecturaAgave/DistV1/Configuracion/cert/llavePublica.jks";

	public static void main(String[] args) {
		
		
		try {
			Signature signer = Signature.getInstance("SHA256withRSA");
			System.out.println(signer.getProvider().getName());
			System.out.println(signer.getAlgorithm());
			PrivateKey privateKey =obtenerLlavePrivadaKS();
			signer.initSign(privateKey);
			
			BeanEntrada entrada = new BeanEntrada();
			entrada.setFechaHora("14/12/2016 18:01:08");
			entrada.setIdUsuario("operador0125");
			entrada.setIdentificador("ABCD1234567F8");
			entrada.setTipoPersona("fisica");
			String cadenaOriginalDelMensajeDeConsulta = "|"+entrada.getFechaHora()+"|"+entrada.getIdUsuario()+"|"+entrada.getIdentificador()+"|"+entrada.getTipoPersona()+"|";
			System.out.println(cadenaOriginalDelMensajeDeConsulta);
			
			byte[] message = cadenaOriginalDelMensajeDeConsulta.getBytes();
			signer.update(message);
			byte [] signature = signer.sign();
			
			System.out.println(signature.length);
			System.out.println(new String(signature));
			System.out.println("======= BASE 64 ==========");
			byte [] result = Base64.encodeBase64(signature);
			String resultB64 = new String(result);
			entrada.setFirma(new String(resultB64));
			entrada.setNumSerieCrt("00009000010000000125");
			entrada.setInfoFirmado("SHA256withRSA");
			System.out.println(entrada);
			
			BeanSalida salida = new BeanSalida();
			salida.setAdvertenciaCliente("Usted está consultando información de una persona que no es cliente de su institución financiera. Este tipo de consultas requieren que cuente con autorización firmada por el usuario");
			salida.setCliente("f");
			salida.setCodigoRespuesta("0");
			salida.setCveCifrada("TYAAgHOvOcy9OTW12MiuLUfISMl6hz79H7uoSqu7uBGkz5u4b4AGbhT4L4lVMluWQWNRwBgmHofu1uyE7tvvgno++yb1VD6GgIjLquAFoZRpb+0Tlk+wJQ7DrfaCuzJOXfXI++Qp3GIfp7NoKig2o1jSfcqYyQ8OiGP2l2HmdG20kjqvrdUOCLOhD9OK/8PYSVT6Ij0P2glzY+HmQDqyekDwXnKSdZ4bo4C8lPd+3JXXnQmgsYVRYeDDZLv1qjaZzidmwZozDpFKWMTCKZQIkG8Ub8uPqJ3cjZyIq7bWgx0zie6u6fzwxTjlcnWNA20UxYdGEtWCRtc+lFmTAlur5w==");
			salida.setFechaHora("14/12/2016 18:01:09");
			salida.setFirma("BuJkiwp4TN4DQ5ZSRD8EIoFB1Jkhy0viJNqDvQo4jJDRBiU9/JHhTMvU2Fl2hfD3Cv5lLVvgYyO4ZN3V6eRbjakjr9qQsCpd3JoZqmvhada23Tuz4bPfvOpkfts7abcFrIycA18oohiQVbas4OQWf80Pzn1zZaz1eTJ8sGQpVxvKDKM/S0LJpGUHcjCOrVLrJtAIjYTwvCvXDa9Em6Y6/U80alF8G2INaTMpHJO6RVON3QUogGY3pT0RAVMzf6zUHOXv+gMxW9lCEgEZ5gCNZIOe/8Myz+cQ/zGpTT+GT6tok1ZDLHqVHMxXnrwQwmRjsGMeP3K/xkPPU2/FT6N02A==");
			salida.setInfoFirmado("SHA256withRSA");
			salida.setMensajeRespuesta("Consulta exitosa.");
			salida.setNumSerieCrt("00009000010000000125");
			salida.setXmlCifrado("nplyggDoGuwoyNWi3Ty3EhQf/Jbku6Uzk3N/9ksoVTjPlT4mVe+KgUILDlgh5lNrdPGdZJWi3UEZxgAvKHKu9tyiylKasAvnrC49ye3cddRu8dE9Itg71xAqCvgRB7MbBSsaOz/NnCJPVsKiDNLYx1YTKQIAX7uRTwBWkLFpSwik3oYxwXiINMYIr6PHp/oBV/WcJYJtto0W6NPNmA7NqBC0CcrCbbKWdFS4P+RbsjZWkGBi9iijaXXcjpB67FLFtYM6KpCF04ughQRD0Px9m8C9VnUXnsFA+wl2AnNXhSgoDEXLU4WaD20zzNnU3qVy6U7CeHV7sZBk9EFhk646bYmrOel0FTKbbH8iNnJZgX/AI306O06wGhYaqx7cUZtixIehLyBPfqJ6epUl/ytSvPuuZBBbZpD5uS2o/zqeMDOEOWYE3SbDx2bjJtptkUb3E2T7mBKYhJS7/AsKPjFzLQb2Ie9XNCCcPR77qSvvk44JddIHmWiDHCkFf3raGl6Eq4KiMYPDbtWhy+gQPNgN9wvzLCx/iYpxjpbO56aTYFYYxDrPdHH3LMhMZzmxlgkrUFxzUMJcZJzqdkuFj0R4vKjOVDg6if2/plcm5TlPnTG4ySd06PWjqLsElPEteIioZdF6MlNpDVAd75Ajc2YLYFTRQLuvfCqJktP11Lp5uCjfFVL5G1+yx29YBl0K3ClSCRt1ZE8w/d7pyIW9I6LYcA/FwhLYDJtBEMKRKE4nNWiPmxmQ5O7N1P39dphbfU4Mstg/+/SI2gdilDaoCQAy1Zr43BNwqLDUbx+Vjqjnf9I0PdYxYH2aUpV6Zo+SNze3gAz29fe5mEE1j2+kzJpfErv6GJRhAWSGg1qHaz414Sn1r9GKThoNkZWOwXJTwY9ZSLuxBDoljOsYxAGPIVjL29jm+mw27T5p1tZw604OUaeTJHYfirF9S6RVGh7EFkQts6BhWjKUWZBl5fz3/2Aguz1P6/N08C0R0/94dLrebNKF7Q4Tth49bTuo4IwvOs2OFHoEdENZFmePZZ/pSAqdPqdcxh+4DDHPAQpHaxH2+6BAaG+x9MLmkeK+GeX9NQACg1F/N11sxaau1TbTHBEWxMGnVnKJsfWLN1WqHJVt3izB3euKvQeQ5sXjceiaVre16OKdsNC8Q2Xme4kjMlGUn44iF7mTttNrOsahLMYsqzyq5/rbtCgb138gF5nviitXpklnCHIeb9fj1YBuGRTKQuU2QW4HcmVuu58EXTG5ncRHFKH0iGgUfFcfU3QtmLOh4huaPhhl40QBj11zs70QZivAdeV+EhrfsP9SvYzhNguh1chnwYPJNgohGO5DSo2lXNxpJVdivXPAfatXFMvGYJQc4JRvuPSRdMttpB57BdLvlibBMkKeDlS0D2RFTPEyGRC3vBF0/Th+rcaGq+y8Xx9rO5IrnvBWyOGD/h5jb0arDMB+++l9qHH20CLCWJXurof8Qlg87JmrisDp6xDwh+VQUaZxLmtkNoG3Ne6dmuFEpSD8rEF841xnGhe5XgCuHvkyIokUCj1LOEnp8eCmp5N5FpJoKoSidjt70F9dnCHLZBCuetwISWOwnzXZDoITU0P1NfPXDEoPHO+wgoY+wzWJSt65V43j8KLhdMaAqRgVgNb8a6tn/btb6+c7b9tME0o++oYC3YLB0mgKamx+YV78+L34WgrQA0w/+XLTPInctqsUBwaGTm268QzZZCU3NZJm6B2ZnEihjfBz+TPRNe5AT9FLUSPK+tl9XJjU2+khb5r2YPTIM/5fYQKBZRUfhy9h+f3IQkkc7l0ritX4dXVV0gNUf8i01qKpa4QrrujSXm+Tu935+4RR1wvN8dcAdn2yr4qWXXiqzGJkuK45t/LUTdx7AXN39xShabKvCSNpa5iwNkmKwWC6/jQ0AxPJV+XR7aH3cdoBP+3+wd9UTtQKqBTCxBacPODQzoPqjn2VkRzpD6YyaUNpQph6EvsXFfcsotz5155bwUmm8I2voMGsIaJwIjND11y+PlZOkyjutgcm4JAAhvAf96Smb4CLg0tQsSPIaWljUl9nSROngMI+cqfPZqzaK7R1RVjrFPo1r9JPxiig96w7p5cwKVcOW4bGj/i4uUfpkZdkktll6MqPjIG7UH2flDL9x4k7MyQQEghUMBUUp1SZ9xbpwK6ekozpIFxQ97HiyHGRjPN8dsULXOTdp8NEYyqw0UGSYJP94bREYYHPcYDC51mqjxMI");
			
			
			//|xmlCifrado|cveCifrada|fechaHora|cliente|
			
			String cadenaOriginalDelMensajeDeSalida = "|" + salida.getXmlCifrado() + "|" + salida.getCveCifrada()+"|"+salida.getFechaHora()+"|"+salida.getCliente()+"|";
			System.out.println(cadenaOriginalDelMensajeDeSalida);
			
			
			byte [] resultDecode = Base64.decodeBase64(salida.getCveCifrada().getBytes());
			System.out.println(new String(resultDecode));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (SignatureException e) {
			e.printStackTrace();
		}
		
		
		/*Provider [] providers = Security.getProviders();
		for (Provider provider : providers){
			System.out.println("Name of Provider: " + provider.getName());
		}*/
		

	}
	
	 private static  PrivateKey obtenerLlavePrivadaKS()  {
			FileInputStream readStream = null;
			PrivateKey privateKey = null;
			
			try {
				// ==== SE LEE EL KEYSTORE (JSK) PRIVADO
				readStream = new FileInputStream(RUTA_JKS_LLAVE_PRIVADA);
				
				// ====== KEYSTORE ======
				KeyStore ks = KeyStore.getInstance(TIPO_KEYSTORE);
				ks.load(readStream, PASS_KEYSTORE.toCharArray());
				
				// KEY
				Key tmpKey = ks.getKey(ALIAS_KEYSTORE, PASS_KEYSTORE.toCharArray());
				if(tmpKey instanceof PrivateKey){
					privateKey = (PrivateKey)tmpKey;
				}
			} catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
				if (e instanceof UnrecoverableKeyException){
					System.err.println("No es posible recuperar el key del KS");
				}if (e instanceof KeyStoreException){
					System.out.println("Error leyendo el keystore");
				}if (e instanceof NoSuchAlgorithmException){
					System.err.println("Algoritmo no soportado");
				}else if (e instanceof CertificateException){
					System.err.println("Error al leer el certificado");
				}else if (e instanceof FileNotFoundException){
					System.err.println("Error al leer");
				}else if (e instanceof IOException){
					System.out.println("Error al descifrar el password");
				}
			}finally{
				try {
					readStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return privateKey;
		}

}
/*
 * Signature signer = Signature.getInstance("SHA1withRSA");
signer.initSign(privateKey);
signer.update(message);
byte[] signature = signer.sign();

 */
