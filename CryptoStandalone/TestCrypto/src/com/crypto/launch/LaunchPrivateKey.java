package com.crypto.launch;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

public class LaunchPrivateKey {
	
	final static String PEM_RSA_PRIVATE_START = "-----BEGIN RSA PRIVATE KEY-----\n";
	final static String PEM_RSA_PRIVATE_END = "-----END RSA PRIVATE KEY-----";

	public static void main(String[] args) {
		
		String cve = "/arquitecturaAgave/DistV1/Configuracion/keys/llaves/VALTIERRA.cve";
		File file = new File(cve);
		//PrivateKey pk = loadPrivateKeyPKCS1(file);
		LaunchPrivateKey launch = new LaunchPrivateKey();
		launch.loadPrivateKeyPKCS1(file);
		

	}

	private  PrivateKey loadPrivateKeyPKCS1(File file) {
		Path path = Paths.get(file.getAbsolutePath());
		String privateKeyPem;
		try {
			//privateKeyPem = new String(Files.readAllBytes(path));
			//privateKeyPem = privateKeyPem.replace(PEM_RSA_PRIVATE_START, "").replace(PEM_RSA_PRIVATE_END, "");
			//privateKeyPem = privateKeyPem.replaceAll("\\s", "");
			//System.out.println(privateKeyPem);
			privateKeyPem = "OS1uFTcQ4D8WTqZneehVYb50I5/+O4l9s3FEeOfZc2kMSB3qGnGyG8v0DQvzKqWDT/6pXFyv+GcntzBz+edR8PUqfSVOnLSh9snXLGmeyoNAl4MqCWOHu8Hp7i/44cyTXDlCgvD6vW7HpVj64bWigHpbzFQfdeB/DJgAVaqLEeQytMvXN8GaIR/eUsUkBKt7WPxlA3XwHyYCQxajZrrOzwkfGcdl72CZGxKOS4fhK68T0qX1CoFbpdZhcCW2/yp/IneZzjkEiQpFu4hZd39TAP5IatyR4BnY2QBoBzYHburL4Ef5WKGSHfTdny2+Q63xt/WSRz/jX2XLrWvJPw/BOUlAUloFZ8zyRwQuwsIniG343Be09gFcTTMpCAP3FF7Ty6UWxmQWtwtbF8goIMt7ggr9908BDaPPZMtOIzyRXrJIwFlgZLxQtK9lCHQcLa6UXEDZaptPgYEmE7LbeA+bUDvYt2t2RkviOI7DoDvbphOhIT0oA85owp8WJH6hdmIkKxaEvkYLzzditemVRLkdJRnJTX0RGeQSrzc0HsSpOfWv958vmdnGeEBWqPCAi27gRW9ducPkF0vYTCukF5SCnVmuxGlNSEAkJARx9NPtNIUo3b+rZSG0MKXjXNcPYtsVp7kdi0sn46jehFcXVFyQNg2RRvbgi8WFw7LzJht0XhzV+xMzFKUStHQJiXOWGImKo6a16mc4ShtPqzczegOiOeScuraW/SSiuHWFYV49tcCg5xjIUSrCmIlxAC+vWLLUZoiBEzbcR5X6VTyf5+E9kHX6CXdFU34EvStm0KIW70UmMKwjI96Eflc7szDSfvMgzPv3D76PsFHIgZBbKoV75evk//zqIbYyp/4Joa/Q6a5EfFnt5xMO/EULaN5hqLz0++w8AocyaU7k6DlefUCgJl2hulmgwspmwtddxglrr5bhRXi81rdl7HTrm+kLBl+tSRfM1ZbACZgi1oczj/MMBNYCevRjlNw+TXkwu3f2Ofd9p2hbctbNamiWemS3RkDEDi0p6HtwcStlHPiFfZA27kE+vVoF9+/3QT2KPya/shyrw17986lV+nVyMB7RJ4Q/+RcVIlZKkZk4RF2V8ghKvnMP/bw3wiHix1UcC15WjUdP+4LIvTrWXrN6MH5cY6i87QaBvCGLnyFJxzDa2GsUssMx39pNxKyiLUnK30ihOWxmqK5lG+JSAM5D4pmQslZ2b0+FxrmALNw/32daDjGoHyXb/sUNdXhkVKDKnqgdRTqf9ERi5kwgV8kh8G4Ytn3oTOigs9tSsoK4p+6JYpeviIUI3Iv1ynRPG+3gMjwwCmqHz8B4mMP1NqWjPimRZLNEO1fCnfeB1evyw/P8ERWgjIXbwxFxfqpKVTWIT36qxSg+kbL3qCc49e0NwZ8bfYynI8bi8auqEP8l9HSz3exApj3qhXCuHKhCIIt0WDYQDRGZnL+Ov4yA+pYxAZxEGrvOkF358p3q5+vGOC53/Dsf3gV3dUkpM8VpiNPGgmWLry+cpYc7aJST2d1mg9aoTc6Ymg4jamFUd8Upre5wz40kcgzskWyA8xJ/UW7tXKFQ5LScnBsedUmWswFsX/CflmEy";
			
			byte [] decode = Base64.getDecoder().decode(privateKeyPem);
			PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(decode);
			
			//SHA256withRSA
			//
			KeyFactory kf = KeyFactory.getInstance("RSA");
			
			
			return kf.generatePrivate(spec);
			
			
			
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			System.err.println(e);
			e.printStackTrace();
		}

		
		return null;
	}
	
	
}
