<!DOCTYPE html> 
<!-- Doctype HTML5 Para mayor infomacion https://www.w3.org/QA/2002/04/valid-dtd-list.html -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
	<head>
		<meta http-equiv="Content-Type"     content="text/html; charset=ISO-8859-1">
		<meta http-equiv=" content-language" content="${pageContext.response.locale}">
		<meta name="_csrf" content="${_csrf.token}" />
		<!-- default header name is X-CSRF-TOKEN -->
		<meta name="_csrf_header" content="${_csrf.headerName}" />
		<title>Prueba Validacion</title>
		<link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
		<link href="${pageContext.servletContext.contextPath}/css/estilosFormulario.css" rel="stylesheet" type="text/css"/>
		<script src="${pageContext.servletContext.contextPath}/js/core/bootstrap.min.js" type="text/javascript" defer></script>
		<script src="${pageContext.servletContext.contextPath}/js/core/jquery.min.js" type="text/javascript" defer></script>
		<script src="${pageContext.servletContext.contextPath}/js/transacciones/transacciones.js" type="text/javascript" defer></script>
	</head>
	<body>
		<div class="container">
			<div class="row">
			  	<div class="col-md-12">
					<div class="col-md-12">
						<h3 style="text-align: center">
							Base de Datos Transferencias Transnacionales <br>
							Consulta Individual de clientes 
						</h3>
					</div>
					
					<div class="form-group">
						<h4>Ingrese los datos para la consulta: </h4>
						<div class="select">
							<select id="tipoPersona" class="form-control">
								<option value="" selected disabled>Seleccione Tipo de Persona</option>
								<option value="fisica" selected="selected">Fisica</option>
								<option value="moral">Moral</option>
							</select>
						</div>
						
						<div class="radio" style="text-align: center;">
							<label class="radio-inline"><input type="radio" name="nacionalidad" id="nacionalidad" value="nacional">Nacional</label>
							<label class="radio-inline"><input type="radio" name="nacionalidad" id="nacionalidad" value="extranjero">Extranjero</label>
						</div>
						
						<div class="input">
							<input type="text" id="idPersona" class="form-control" placeholder="Capture CURP" />
						</div>
						
						<div class="input">
							<input type="text" id="idUsuario" class="form-control" placeholder="ID Usuario" />
						</div>
						
						<div class="col-lg-6 col-sm-6 col-12">
				            <div class="input-group">
				                <label class="input-group-btn">
				                    <span class="btn btn-primary">
				                        Browse... <input type="file" id="certificado" style="display: none;" multiple="">
				                    </span>
				                </label>
				                <input type="text" class="form-control" readonly placeholder="Certificado Usuario (.cer)">
				            </div>
				        </div>
						
						<button type="button" onclick="doAjax()" class="btn btn-primary">Entrar</button>
						<br />
						<div id="error" class="error"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>