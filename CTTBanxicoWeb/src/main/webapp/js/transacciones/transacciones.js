/**
 * Javascript de modulo de transacciones. Ejemplo de validacion ajax con JSR 303.
 */
function doAjax() {
	var tipoPersona = $('#tipoPersona').val();
	var nacionalidad = $('#nacionalidad:checked').val();
	var idPersona = $('#idPersona').val();
	var idUsuario = $('#idUsuario').val();
	var certificado = $('#certificado').val();
	// Elementos necesarios para evitar CSFR
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");

	$.ajax({
		type: "POST",
		// Elementos necesarios para evitar CSFR
		beforeSend: function (request) {
			if(token){
				request.setRequestHeader(header, token);
			}
		},
		url: "/CTTBanxicoWeb/domicilios/guardarDomicilio.do",
		data: "tipoPersona=" + tipoPersona + "&nacionalidad=" + nacionalidad + "&idPersona=" + idPersona + "&idUsuario=" + idUsuario + "&certificado=" + certificado,
		success: function(response){
			if(response.status == "SUCCESS"){
				alert("Exito!!");
				$('#error').hide('slow');
			}else{
				$('#error').html("");
				var errorInfo = "";
				for(var i =0 ; i < response.result.length ; i++){
					errorInfo += "<br>" + (i + 1) +". " + response.result[i];
				}
				$('#error').show('slow');
				$('#error').html("Por favor corrija los siguiente errores: " + errorInfo);

			}
		},
		error: function(e){
			alert('Error: ' + e);
		}
	});
	
}
