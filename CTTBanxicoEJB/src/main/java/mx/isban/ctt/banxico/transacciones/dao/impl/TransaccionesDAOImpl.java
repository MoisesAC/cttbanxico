package mx.isban.ctt.banxico.transacciones.dao.impl;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.ctt.banxico.transacciones.dao.TransaccionesDAO;

@Stateless
@Local(TransaccionesDAO.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class TransaccionesDAOImpl extends Architech implements TransaccionesDAO {

	/** serialVersionUID. */
	private static final long serialVersionUID = 6410828904700918810L;

}
