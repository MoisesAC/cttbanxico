package mx.isban.ctt.banxico.transacciones.ejb.impl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.ctt.banxico.transacciones.ejb.TransaccionesBO;

@Stateless
@Remote(TransaccionesBO.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class TransaccionesBOImpl extends Architech implements TransaccionesBO {

	/** serialVersionUID. */
	private static final long serialVersionUID = -4096814083322256201L;

}
