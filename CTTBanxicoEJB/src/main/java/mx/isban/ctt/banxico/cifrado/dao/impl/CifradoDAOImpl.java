package mx.isban.ctt.banxico.cifrado.dao.impl;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.ctt.banxico.cifrado.dao.CifradoDAO;

@Stateless
@Local(CifradoDAO.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class CifradoDAOImpl extends Architech implements CifradoDAO {

	/** serialVersionUID. */
	private static final long serialVersionUID = 1156199246029428481L;

}
