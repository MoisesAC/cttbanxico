package mx.isban.ctt.banxico.cifrado.ejb.impl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.agave.commons.architech.Architech;
import mx.isban.ctt.banxico.cifrado.ejb.CifradoBO;

@Stateless
@Remote(CifradoBO.class)
@TransactionManagement(TransactionManagementType.BEAN)
public class CifradoBOImpl extends Architech implements CifradoBO {

	/** serialVersionUID. */
	private static final long serialVersionUID = 5931109035184971706L;

}
